const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers");
const auth = require("../auth")

router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({auth: "You're not an admin"})
	}	
});

//retrieving all courses
router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
});

//retrieving all active courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});

//retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
})

//update a course
router.put("/:courseId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )

//archiving 
router.put("/archive/:courseId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )

module.exports = router;