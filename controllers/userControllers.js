const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth")

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0 ){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error) {
			return false;
		} else {
			//User Registration is success
			return true
		}
	})

}

//user authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		};
	});
};

//getProfile s33 activity

module.exports.getProfile = (userProfile) =>{
	return User.findById(userProfile).then(profile => {
		
		profile.password = "******";
		return profile;
	});
}

//Async and await - allow the processes to wait for each other


module.exports.enroll = async (data) => {
	//Add the courseId to the enrollments array of the user

	let isUserUpdated = await User.findById(data.userId).then( user => {
		//push the course Id to enrollments property

		user.enrollments.push({ courseId: data.courseId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else {
				return true
			}
		})
	});


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//add the userId in the course's database(enrollees)
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});


	//Validation
	if(isUserUpdated && isCourseUpdated){
		//user enrollment successful
		return true;
	}else {
		//user enrollment failed
		return false;
	}

}











































