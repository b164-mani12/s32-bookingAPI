const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false;
		}else{
			return true;
		}
	})
}

//retrieving all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//retrieving all active courses
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}

//retrieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}

module.exports.updateCourse = (courseId, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

//archive
module.exports.archiveCourse = (courseId, reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(courseId, archiveCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}


//enrolllments



































